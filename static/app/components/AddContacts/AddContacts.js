angular.module('imenikApp').component('addContacts', {
    templateUrl: '/app/components/AddContacts/AddContacts.html',
    controller: function (ContactsService) {
        const viewmodel = this;

        viewmodel.contactObj = {
            "name" : "",
            "surname" : "",
            "mobile" : "",
            "home" : "",
            "work" : "",
            "emailAddress" : ""
        },

        viewmodel.addContacts = function () {
            ContactsService.addContacts(viewmodel.contactObj).then(function (response) {
                console.log(response);
            });
        }
    },
    controllerAs: 'controller'
});