angular.module('imenikApp').component('contactsList', {
    templateUrl: '/app/components/ContactsList/ContactsList.html',
    bindings: {
        contacts: '<'
    },
    controller: function () {
        const viewmodel = this;

        viewmodel.contactsList = [];

        viewmodel.$onInit = function () {
            viewmodel.contactsList = viewmodel.contacts.data;
        }
    },
    controllerAs: 'controller'
});