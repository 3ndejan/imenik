angular.module('imenikApp').component('contactsDetails', {
    templateUrl: '/app/components/ContactsDetails/ContactsDetails.html',
    bindings: {
        contact: '<'
    },
    controller: function (ContactsService, $uiRouterGlobals, $state) {
        const viewmodel = this;

        viewmodel.contactObj = [];

        viewmodel.$onInit = function () {
            viewmodel.contactObj = viewmodel.contact.data[0];
        }

        viewmodel.delete = function () {
            ContactsService.deleteContacts($uiRouterGlobals.params.contactId).then(function (response) {
                console.log(response);

                $state.transitionTo('ContactsList', { 
                    reload: true, inherit: false, notify: true
                  });
            });
        }
    },
    controllerAs: 'controller'
});