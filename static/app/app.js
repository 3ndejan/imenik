var imenikApp = angular.module('imenikApp', ['ui.router']);

imenikApp.service('ContactsService', function ($http) {
    this.fetchContacts = function () {
        return $http.get('/api/fetch/contacts');
    }

    this.fetchSingleContact = function (contactId) {
        return $http.get('/api/fetch/contacts', {params: {'contactId': contactId}});
    }

    this.addContacts = function (contactObj) {
        return $http.post('/api/create/contact', contactObj);
    }

    this.deleteContacts = function (contactId) {
        return $http.delete('/api/delete/contact', {params: {'contactId': contactId}});
    }
});

imenikApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('ContactsList', {
        url: '/',
        component: 'contactsList',
        resolve: {
            contacts: function (ContactsService) {
                return ContactsService.fetchContacts();
            }
        },
        cache: false
    })
    .state('ContactsList.Details', {
        url: '/contacts/{contactId}',
        component: 'contactsDetails',
        params: {
            contactId: null
        },
        resolve: {
            contact: function (ContactsService, $transition$) {
                return ContactsService.fetchSingleContact($transition$.params().contactId);
            }
        }
    })
    .state('ContactsList.Add', {
        url: '/contacts/add',
        component: 'addContacts'
    });

    $urlRouterProvider.otherwise('/');
}]);