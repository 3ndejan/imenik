from flask import Flask, send_file, request, jsonify, abort

from flaskext.mysql import MySQL
from flaskext.mysql import pymysql

app = Flask("Imenik", static_url_path="")

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "toor"
app.config["MYSQL_DATABASE_DB"] = "phonebook"

mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

@app.route('/')
def index():
    return send_file("index.html")

@app.route('/api/fetch/contacts', methods=['GET'])
def fetchContacts():
    cursor = mysql.get_db().cursor()
    
    if not request.args:
        cursor.execute("SELECT idContacts, name, surname, photo, Groups_idGroups, favorite FROM contacts")
        contacts = cursor.fetchall()

        return jsonify(contacts), 200
    
    elif "contactId" in request.args:
        cursor.execute("SELECT * FROM contacts WHERE idContacts = %(contactId)s", request.args)
        contact = cursor.fetchall()

        return jsonify(contact), 200
    
    else:
        abort(404)

@app.route('/api/create/contact', methods=['POST'])
def addContacts():
    cursor = mysql.get_db().cursor()
    cursor.execute("""INSERT INTO contacts (name, surname, mobile, home, work, emailAddress, photo, favorite, Groups_idGroups)
    VALUES (%(name)s, %(surname)s, %(mobile)s, %(home)s, %(work)s, %(emailAddress)s, DEFAULT, DEFAULT, DEFAULT)""", request.json)
    mysql.get_db().commit()

    return "Created.", 200

@app.route('/api/delete/contact', methods=['DELETE'])
def deleteContacts():
    if "contactId" in request.args:
        cursor = mysql.get_db().cursor()
        cursor.execute("DELETE FROM contacts WHERE idContacts = %(contactId)s", request.args)
        mysql.get_db().commit()
    
        return "Deleted.", 200
    
    else:
        abort(404)